/* pathdef.c */
/* This file is automatically created by Makefile
 * DO NOT EDIT!  Change Makefile only. */
#include "vim.h"
char_u *default_vim_dir = (char_u *)"/usr/local/share/vim";
char_u *default_vimruntime_dir = (char_u *)"";
char_u *all_cflags = (char_u *)"gcc -c -I. -Iproto -DHAVE_CONFIG_H   -DMACOS_X_UNIX -no-cpp-precomp  -O2 -fno-strength-reduce -Wall -U_FORTIFY_SOURCE -D_FORTIFY_SOURCE=1      ";
char_u *all_lflags = (char_u *)"gcc   -L. -L/usr/local/lib -L/usr/local/opt/libyaml/lib -L/usr/local/opt/readline/lib -L/usr/local/opt/libksba/lib -L/usr/local/opt/openssl/lib   -L/usr/local/lib -o vim        -lm -lncurses  -liconv -framework Cocoa       -lruby.1.9.1 -lobjc -L/Users/steve/.rvm/rubies/ruby-1.9.3-p484/lib   ";
char_u *compiled_user = (char_u *)"steve";
char_u *compiled_sys = (char_u *)"Steve-Martins-MacBook-Pro.local";
